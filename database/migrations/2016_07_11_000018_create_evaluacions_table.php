<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->integer('ponderacion');
            $table->dateTime('fecha_hora');
            $table->boolean('presentado')->default(0);
            $table->integer('materia_id')->unsigned();
            $table->foreign('materia_id')
              ->references('id')->on('materias')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluacions');
    }
}
