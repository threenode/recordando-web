<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \App\User;
use \App\Materia;
use \App\Evaluacion;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      ///// USUARIOS
      $users = [
        [
          'id'       => '1',
          'name'     => 'Jose Salazar',
          'email'    => 'jsalazar3300@gmail.com',
          'password' => Hash::make('jose'),
        ],
        [
          'id'       => '2',
          'name'     => 'Piero Pinzon',
          'email'    => 'pieropinzon@gmail.com',
          'password' => Hash::make('piero'),
        ],
        [
          'id'       => '3',
          'name'     => 'Natasha Mendez',
          'email'    => 'natashamendez@gmail.com',
          'password' => Hash::make('natasha'),
        ]
      ];

      foreach ($users as $key => $user) {
        User::create($user);
      }
      ///// USUARIOS


      ///// MATERIAS
      $materias = [
        [ 'id'		 => '1',
          'nombre'   => 'Ingenieria Economica',
          'profesor' => 'Christian Cedeño',
          'seccion'  => '1',
          'user_id'  => '1', //  JOSE
        ],
        [ 'id'		 => '2',
          'nombre'   => 'Sistemas Distribuidos',
          'profesor' => 'Virginia Padilla',
          'seccion'  => '2',
          'user_id'  => '1', //  JOSE
        ],
        [ 'id'		 => '3',
          'nombre'   => 'Tendencias Informaticas',
          'profesor' => 'Isabel Garcia',
          'seccion'  => '1',
          'user_id'  => '1', //  JOSE
        ],
        [ 'id'		 => '4',
          'nombre'   => 'Seminario de Trabajo de Grado',
          'profesor' => 'Luz Medina Prada',
          'seccion'  => '1',
          'user_id'  => '1', //  JOSE
        ],
      ];

      foreach ($materias as $key => $m) {
        Materia::create($m);
      }
      ///// MATERIAS

      ///// EVALUACIONES
      $evaluaciones = [
        [
          'id'          => '1',
          'tipo'        => 'Examen',
          'ponderacion' => '10',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '1',
        ],
        [
          'id'          => '2',
          'tipo'        => 'Exposicion',
          'ponderacion' => '20',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '1',
        ],
        [
          'id'          => '3',
          'tipo'        => 'Trabajo',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '2',
        ],
        [
          'id'          => '4',
          'tipo'        => 'Taller',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '2',
        ],
        [
          'id'          => '5',
          'tipo'        => 'Practica',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '3',
        ],
        [
          'id'          => '6',
          'tipo'        => 'Taller',
          'ponderacion' => '10',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '3',
        ],
        [
          'id'          => '7',
          'tipo'        => 'Examen',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '4',
        ],
        [
          'id'          => '8',
          'tipo'        => 'Debate',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '4',
        ],
        [
          'id'          => '9',
          'tipo'        => 'Cartelera',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '1',
        ],
        [
          'id'          => '10',
          'tipo'        => 'Compartir',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '1',
        ],
        [
          'id'          => '11',
          'tipo'        => 'Proyecto',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '1',
        ],
        [
          'id'          => '12',
          'tipo'        => 'Tarea',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '2',
        ],
        [
          'id'          => '13',
          'tipo'        => 'Examen',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '2',
        ],
        [
          'id'          => '14',
          'tipo'        => 'Taller',
          'ponderacion' => '10',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '2',
        ],
        [
          'id'          => '15',
          'tipo'        => 'Exposicion',
          'ponderacion' => '20',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '3',
        ],
        [
          'id'          => '16',
          'tipo'        => 'Practica',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '3',
        ],
        [
          'id'          => '17',
          'tipo'        => 'Examen',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '3',
        ],
        [
          'id'          => '18',
          'tipo'        => 'Proyecto',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '4',
        ],
        [
          'id'          => '19',
          'tipo'        => 'Examen',
          'ponderacion' => '5',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '4',
        ],
        [
          'id'          => '20',
          'tipo'        => 'Examen',
          'ponderacion' => '15',
          'fecha_hora'  => date("Y-m-d H:i:s"),
          'materia_id'  => '4',
        ]
      ];

      foreach ($evaluaciones as $key => $eva) {
        Evaluacion::create($eva);
      }
      ///// EVALUACIONES

      // Model::reguard();
    }
}