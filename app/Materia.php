<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
  protected $fillable = ['nombre', 'profesor', 'seccion', 'user_id'];
  public $timestamps  = false;
  
  public function user()
  {
    return $this->belongsTo('App\User');
  }  

  public function evaluaciones()
  {
    return $this->hasMany('App\Evaluacion');
  }

}
