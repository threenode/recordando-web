<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function materias()
    {
        return $this->hasMany('App\Materia');
    }

    public function evaluaciones()
    {
        return $this->hasManyThrough(
            'App\Evaluacion', 'App\Materia',
            'user_id', 'materia_id', 'id'
        );
    }
}
