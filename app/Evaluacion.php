<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
  protected $fillable = ['tipo', 'ponderacion', 'fecha_hora', 'presentado', 'materia_id'];
  public $timestamps  = false;
  
  public function materia()
  {
    return $this->belongsTo('App\Materia');
  }

  public function user()
  {
    return $this->belongsTo('App\Materia')->getResults()->belongsTo('App\User');
    $materia = $this->belongsTo('App\Materia');
    return $materia->getResults()->belongsTo('App\User');
  }  

}
