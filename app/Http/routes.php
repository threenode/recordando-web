<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'api', 'middleware' => 'cors'], function(){
  Route::post('/auth',     ['as' => 'auth',     'uses' =>'JWTAuthController@auth']);
  Route::post('/register', ['as' => 'register', 'uses' =>'JWTAuthController@register']);
  Route::get('/auth',      ['as' => 'getuser',  'uses' =>'JWTAuthController@getUser']);
  Route::resource('/materia', 'MateriaController');
  Route::resource('/evaluacion', 'EvaluacionController');
  Route::get('evaluacion/{id}/done', ['as' => 'doneEvaluacion',  'uses' =>'EvaluacionController@done']);
  Route::get('evaluacion/{id}/undone', ['as' => 'doneEvaluacion',  'uses' =>'EvaluacionController@unDone']);
});
