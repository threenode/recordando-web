<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use JWTAuth;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use Exception;
use Validator;


class JWTAuthController extends Controller
{
  //  METODO DE AUTENTIFICACION
  public function auth(Request $request){
    $credentials = $request->only('email', 'password');
    try{
      if ( !$token = JWTAuth::attempt($credentials) ){
        return response()->json(['error' => 'Combinacion usuario, contraseña incorrecta'], 401);
      }
    } catch (JWTException $e){
      return response()->json(['error' => 'No se pudo crear el token'], 500);
    }
    return response()->json( compact('token'), 200);
  }

  //  RETORNA USUARIO DE TOKEN
  public function getUser()
  {
    try {
      if (! $user = JWTAuth::parseToken()->authenticate()) {
        return response()->json(['error' => 'Usuario no encontrado'], 404);
      }
    } catch (TokenExpiredException $e) {
      return response()->json(['error' => 'Token expirado'], $e->getStatusCode());
    } catch (TokenInvalidException $e) {
      return response()->json(['error' => 'Token invalido'], $e->getStatusCode());
    } catch (JWTException $e) {
      return response()->json(['error' => 'Token ausente'], $e->getStatusCode());
    }
    return response()->json(compact('user'));
  }

  //  REGISTRAR NUEVO USUARIO
  public function register(Request $request){
    $newuser  = $request->all();
    $validator = Validator::make($newuser, [
      'name'     => 'required',
      'email'    => 'unique:users',
      'password' => 'required',
      ]);
    if ( $validator->fails() )
      return response()->json([
        'success' => false,
        'errors'  => $validator->getMessageBag()->toArray()
        ], 400);
    else{
      $newuser['password'] = Hash::make($request->input('password'));
      return User::create($newuser);
    }
  }

}
