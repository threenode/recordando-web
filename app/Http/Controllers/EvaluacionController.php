<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Evaluacion;
use JWTAuth;
use Exception;
use Validator;
use DB;
class EvaluacionController extends Controller
{

      //  CONSTRUCTOR
    public function __construct()
    { 
        $this->middleware('jwt.auth');
    }

    // LIST 
    public function index(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $r    = $request->all();

        if ( array_key_exists('materia_id', $r)) {
            return response()->json([
              'data' => Evaluacion::where('materia_id', $r['materia_id'])->get(),
              ], 200);
        } else{
            $evals = DB::table('users')
            ->join('materias', 'users.id',       '=', 'materias.user_id')
            ->join('evaluacions', 'materias.id', '=', 'evaluacions.materia_id')
            ->select("evaluacions.id", "tipo", "ponderacion", "fecha", "hora", "presentado", "materia_id")
            ->where('user_id', $user->id)
            ->get();
            return response()->json([
              'data' => $evals,
              ], 200);
        }
    }

    // DETALLE
    public function show($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json([
          'data' => Evaluacion::where('id', $id)->get()
        ], 200);
    }

    //  GUARDAR
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $validate = $this->validar($request);    
        $newEvaluacion = $request->all();
        if ( !$validate ){
         $e = Evaluacion::create($newEvaluacion);
          return response()->json([
            'message' => 'Materia agregada con exito',
            'data'    => $e
          ], 200);
        } else {
          return $validate;
        }
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
       $user    = JWTAuth::parseToken()->authenticate();
       $evaluacion = Evaluacion::find($id);
       if ( $evaluacion ){
         Evaluacion::destroy($evaluacion->id);
         return  response('Evaluacion eliminada con  exito',200);;
       }else{
        return response('Desautorizado',403);
      }
    }

    //  VALIDA PETICIONES
    public function validar(Request $r){
     $validator = Validator::make($r->all(), [
      "tipo"        => "required",
      "ponderacion" => "required",
      "fecha_hora"  => "required",
      "materia_id"  => "required"
      ]);
     if ( $validator->fails() )
      return response()->json([
        'success' => false,
        'errors'  => $validator->getMessageBag()->toArray()
        ], 400);
    else
      return false;
  }


  public function done($id)
  {
    $user    = JWTAuth::parseToken()->authenticate();
      $evaluacion = Evaluacion::find($id);
      if ( $evaluacion ){
        $evaluacion->presentado = 1;
        $evaluacion->save();
       return  response('Evaluacion modificada con exito',200);;
     }else{
      return response('Desautorizado',403);
    }
  }

  public function unDone($id)
  {
    $user    = JWTAuth::parseToken()->authenticate();
      $evaluacion = Evaluacion::find($id);
      if ( $evaluacion ){
        $evaluacion->presentado = 0;
        $evaluacion->save();
       return  response('Evaluacion modificada con exito',200);;
     }else{
      return response('Desautorizado',403);
    }
  }
}
