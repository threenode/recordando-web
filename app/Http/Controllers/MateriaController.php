<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Materia;
use JWTAuth;
use Exception;
use Validator;

class MateriaController extends Controller
{
  //  CONSTRUCTOR
  public function __construct()
  { 
    $this->middleware('jwt.auth');
  }

  //  LIST MATERIAS
  public function index()
  {
    $user = JWTAuth::parseToken()->authenticate();
    $materias = Materia::where('user_id', $user->id)->get();
    $response = [];
    foreach ($materias as $key => $value) {
      $response[$value["id"]] = $value;
    }
    return response()->json(array(
      'data' => $response
    ), 200);
  }

  //  DETALLE 
  public function show($id)
  {
    $user = JWTAuth::parseToken()->authenticate();
    return response()->json([
      'data' => Materia::where('id', $id)->get()
    ], 200);
  }

  //  GUARDAR MATERIA EN BASE DE DATOS
  public function store(Request $request)
  {   
    $user = JWTAuth::parseToken()->authenticate();
    $validate = $this->validar($request);    
    $newMateria = $request->all();
    $newMateria['user_id'] = $user->id;
    if ( !$validate ){
     $m = Materia::create($newMateria);
      return response()->json([
        'message' => 'Materia agregada con exito',
        'data'    => $m
      ], 200);
    } else {
      return $validate;
    }
  }

  //  ACTUALIZAR MATERIA
  public function update(Request $request, $id)
  {
    $user     = JWTAuth::parseToken()->authenticate();
    $validate = $this->validar($request);    
    $materia = Materia::where('user_id', $user->id)->where('id',$id)->first();
    if ( $materia ){
      $materia->nombre   = $request->input('nombre');
      $materia->profesor = $request->input('profesor');
      $materia->seccion  = $request->input('seccion');
      $materia->save();
      return response()->json([
        'message' => 'Materia modificada con exito',
        'data'    => $materia
        ], 200);
    }else{
      return response()->json(['error' => 'Desautorizado'], 403);
    }
  }

  //  ELIMINAR MATERIA
  public function destroy($id)
  {
     $user    = JWTAuth::parseToken()->authenticate();
     $materia = Materia::where('user_id', $user->id)->where('id',$id)->first();
     if ( $materia ){
       Materia::destroy($materia->id);
       return  response('Materia eliminada con  exito',200);;
     }else{
      return response('Desautorizado',403);
    }
  }

  //  VALIDA PETICIONES
  public function validar(Request $r){
     $validator = Validator::make($r->all(), [
      'nombre'   => 'required|unique:materias',
      'profesor' => 'required',
      'seccion'  => 'required',
      ]);
     if ( $validator->fails() )
      return response()->json([
        'success' => false,
        'errors'  => $validator->getMessageBag()->toArray()
        ], 400);
    else
      return false;
  }
}
