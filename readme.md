# Recordando Laravel App
----------

## Desarrollo
Desde consola, teniendo instalado php y composer ejecutas:

- Dependencias:
```
$ composer install
```

## Artisan Tasks

- Servidor
> 
```
$ php artisan serve
```

- Migrations
> 
```
$ php artisan migrate
```

- dataBase Seed
> 
```
$ php artisan db:seed
```
